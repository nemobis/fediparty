---
layout: "post"
title: "Themed servers"
---

**Unsure where to register to join Fediverse?** Choose a website from this curated list. It includes [Mastodon](/en/mastodon), Glitch-soc and Hometown (Mastodon forks), [Pleroma](/en/pleroma) and Akkoma (Pleroma fork), [Friendica](/en/friendica), [Misskey](/en/misskey) and Calckey (Misskey fork), and [Hubzilla](/en/hubzilla) servers. Websites are not restricted to their theme. They help people with common interests find their community.

<ul class="article-list">

### 💡 [Sciences](#sciences)
* [lugnasad.eu](https://lugnasad.eu) - for French speaking science lovers *(Friendica)*
* [astrodon.social](https://astrodon.social) - for anyone interested in astronomy, astrophysics, astrophotography *(Mastodon)*
* [mstdn.science](https://mstdn.science) - for microbiologists, scientists in general, and science enthusiasts *(Mastodon)*
* [astronomy.city](https://astronomy.city) - for astronomy and astronomy-adjacent users *(Mastodon)*
* [astronomy.social](https://astronomy.social) - astronomy, space travel and related fields of knowledge *(Mastodon)*
* [humangenetics.social](https://humangenetics.social) - for people interested in the science of human genetics *(Mastodon)*

### 🎨 [Humanities](#humanities)
* [oulipo.social](https://oulipo.social) - a lipogrammatic server *(Mastodon)*
* [dju.social](https://dju.social) - German journalists union *(Mastodon)*
* [archaeo.social](https://archaeo.social) - for archaeologists, historians and lovers of all things ancient *(Mastodon)*
* [artisan.chat](https://artisan.chat) - for creators, crafters, and artists *(Mastodon)*
* [zirk.us](https://zirk.us) - for readers, writers, thinkers, artists, academics, enthusiasts in arts and humanities *(Mastodon)*

### 🎓 [Education](#education)
* [tusk.schoollibraries.net](https://tusk.schoollibraries.net) - helping educators improve the School Libraries Resource Network *(Mastodon)*
* [mastodon.oeru.org](https://mastodon.oeru.org) - for educators and learners involved in the [OERu](https://oeru.org) *(Mastodon)*
* [akademienl.social](https://akademienl.social) - for anyone working in or with a general interest in Dutch academia

### 🎵 [Music](#music)
* [koreadon.com](https://koreadon.com) - for K-POP music fans *(Mastodon)*
* [feedbeat.me](https://feedbeat.me) - dedicated to culture and events such as music, poetry, comedy (German) *(Mastodon)*
* [piano.masto.host](https://piano.masto.host) - dedicated to piano music, in English/Japanese *(Mastodon)*

### 🔭 [Interests and hobbies](#hobbies)
* [screenwriting.space](https://screenwriting.space) - a place for storytellers *(Mastodon)*
* [rollenspiel.social](https://rollenspiel.social) - (German) roleplay, Pen & Paper, tabletop, TCG, for all gamers *(Mastodon)*
* [radiosocial.de](https://radiosocial.de) - for German radio amateurs *(Mastodon)*
* [hamradio.tel](https://hamradio.tel) - ham radio community *(Mastodon)*
* [pl.nudie.social](https://pl.nudie.social) - for naturists, nudists, clothes-free living *(Akkoma)*
* [prf.me](https://prf.me) - for perfume, fragrance and scent lovers *(Mastodon)*
* [makerspace.social](https://makerspace.social) - space for makers (CNC, woodworking, microcontrollers, etc) *(Mastodon)*
* [3dp.chat](https://3dp.chat) - 3D printing *(Mastodon)*
* [bikesare.cool](https://bikesare.cool) - space for competitive and casual cyclists *(Mastodon)*
* [graphics.social](https://graphics.social) - for the computer graphics community *(Mastodon)*
* [cartoonist.social](https://cartoonist.social) - community for cartoonists *(Mastodon)*
* [retro.pizza](https://retro.pizza) - for nerds to talk about nerd culture *(Mastodon)*
* [onlycosplays.social](https://onlycosplays.social) - safe place for cosplayers and their communities *(Mastodon)*
* [social.vtopia.live](https://social.vtopia.live) - for Vtubers, their fans, and the creators that make them possible *(Misskey)*
* [gametoots.de](https://gametoots.de) - for gamers and streamers *(Mastodon)*

### 🚄  [Travel, Transport and Infrastructure](#travel)
* [rail.chat](https://rail.chat) - discussions about long-distance, passenger and freight rail networks for economic, environmental and equity benefits *(Mastodon)*
* [aircrew.rocks](https://aircrew.rocks) - for pilots, flight attendants, and flight enthusiasts *(Mastodon)*
* [aviators.network](https://aviators.network) - for aviation enthusiasts *(Mastodon)*
* [bahn.social](https://bahn.social) - for rail enthusiasts, in German *(Mastodon)*
* [matitodon.com](https://matitodon.com) - exploring geography (traffic and towns), in Japanese *(Mastodon)*
* [urbanists.social](https://urbanists.social) - for people who like bikes, transit, and walkable cities *(Mastodon)*
* [bikejam.social](https://bikejam.social) - for bike and pedestrian infrastructure *(Mastodon)*

### 🎏 [Language specific](#languages)
* [toki.social](https://toki.social) - for those interested in toki pona *(Mastodon)*
* [mastodon.sk](https://mastodon.sk) - for Slovak users *(Mastodon)*
* [mastodon.eus](https://mastodon.eus) - for Euskera/Basque speakers *(Mastodon)*
* [xarxa.cloud](https://xarxa.cloud) - for Catalan and Spanish speakers *(Mastodon)*
* [mastodon.fedi.bzh](https://mastodon.fedi.bzh) - for Breton and Gallo speakers *(Mastodon)*
* [gomastodon.cz](https://gomastodon.cz) - for Czech users *(Mastodon)*
* [mk.phreedom.club](https://mk.phreedom.club) - for Russian users *(Misskey)*
* [librosphere.fr](https://librosphere.fr) - for French speakers *(Pleroma)*
* [mastodon.nu](https://mastodon.nu) - for Swedish speakers *(Mastodon)*
* [norrebro.space](https://norrebro.space) - for Danish speakers *(Mastodon)*
* [loðfíll.is](https://xn--lofll-1sat.is) - for Icelandic speakers *(Mastodon)*
* [tea.codes](https://tea.codes) - for Chinese speakers *(Mastodon)*
* [planet.moe](https://planet.moe) - for Korean speakers *(Mastodon)*
* [mastodon.com.tr](https://mastodon.com.tr) - for Turkish speakers *(Mastodon)*
* [best-friends.chat](https://best-friends.chat) - for Japanese speakers *(Mastodon)*
* [mastodon.in.th](https://mastodon.in.th) - for Thai speakers *(Mastodon)*
* [hub.mtf.party](https://hub.mtf.party) - for Chinese speakers, trans-friendly *(Mastodon)*
* [bluestar.social](https://bluestar.social) - Arabic language *(Mastodon)*
* [occitania.social](https://occitania.social) - community of Occitan culture and language *(Mastodon)*
* [bassam.social](https://bassam.social) - Arabic language *(Mastodon)*

### 🛡 [Safer spaces](#safer-spaces)

Instances run by and for people belonging to some minority and moderated more strictly than usual to cater to it.

* [neovibe.app](https://neovibe.app) - for fans of music, movies, gaming and all forms of entertainment, LGBTQ+ friendly and Black-run *(Mastodon)*
* [neurodifferent.me](https://neurodifferent.me) - a friendly space for neurodifferent folks *(Mastodon)*

### ⛺ [Regional](#regional)

#### Australia
* [bne.social](https://bne.social) - Brisbane *(Mastodon)*

#### Austria
* [krems.social](https://krems.social) - Krems *(Mastodon)*
* [tyrol.social](https://tyrol.social) - Tyrol *(Mastodon)*
* [wien.rocks](https://wien.rocks) - Vienna *(Mastodon)*
* [fedi.at](https://fedi.at) - Austria *(Mastodon)*
* [sbg-social.at](https://sbg-social.at) - Salzburg *(Mastodon)*
* [aut.social](https://aut.social) - Austria *(Mastodon)*

#### Belgium
* [wokka.be](https://wokka.be) - Belgium
* [mastodon-belgium.be](https://mastodon-belgium.be) - Belgium *(Mastodon)*

#### Canada

* [halifaxsocial.ca](https://halifaxsocial.ca) - Halifax *(Mastodon)*

#### Denmark
* [norrebro.space](https://norrebro.space) - Denmark *(Mastodon)*

#### Finland
* [mastodo.fi](https://mastodo.fi) - Finland *(Mastodon)*

#### Germany
* [muenchen.social](https://muenchen.social) - Munich *(Mastodon)*
* [mastodon.bayern](https://mastodon.bayern) - Bavaria *(Mastodon)*
* [ruhr.social](https://ruhr.social) - Ruhr area *(Mastodon)*
* [social.saarland](https://social.saarland) - Saarland *(Mastodon)*
* [dresden.network](https://dresden.network) - Dresden *(Mastodon)*
* [machteburch.social](https://machteburch.social) - Magdeburg *(Mastodon)*
* [fulda.social](https://fulda.social) - Fulda *(Mastodon)*
* [brandenburg.social](https://brandenburg.social) - Brandenburg *(Mastodon)*
* [berlin.social](https://berlin.social) - Berlin *(Mastodon)*
* [osna.social](https://osna.social) - Osnabrück *(Mastodon)*
* [fem.social](https://fem.social) - Ilmenau *(Mastodon)*
* [friendica.a-zwenkau.de](https://friendica.a-zwenkau.de) - Zwenkau *(Friendica)*
* [hessen.social](https://hessen.social) - Hesse *(Mastodon)*
* [frankfurt.social](https://frankfurt.social) - Frankfurt *(Mastodon)*
* [harz.social](https://harz.social) - Harz area *(Mastodon)*
* [rheinneckar.social](https://rheinneckar.social) - Rhein-Neckar area *(Mastodon)*
* [nahe.social](https://nahe.social) - Nahe region *(Mastodon)*
* [moessingen.social](https://moessingen.social) - Mössingen area *(Mastodon)*
* [bremen.team](https://bremen.team) - Bremen *(Mastodon)*
* [chemnitz.network](https://chemnitz.network) - Chemnitz *(Mastodon)*
* [chemnitz.rocks](https://chemnitz.rocks) - Chemnitz *(Friendica)*
* [mfr.social](https://mfr.social) - (Mittel-)Franken *(Mastodon)*
* [cas.social](https://cas.social) - Castrop-Rauxel *(Mastodon)*

#### Netherlands

* [nwb.social](https://nwb.social) - Nieuw West-Brabant *(Mastodon)*

#### Norway
* [oslo.town](https://oslo.town) - Oslo *(Mastodon)*

#### Portugal
* [lisboa.social](https://lisboa.social) - Lisbon *(Mastodon)*

#### Spain
* [barcelona.social](https://barcelona.social) - Barcelona *(Pleroma)*
* [mstdn.es](https://mstdn.es) - Spain *(Mastodon)*

#### Switzerland
* [swiss.social](https://swiss.social) - Switzerland *(Mastodon)*
* [zurich.social](https://zurich.social) - Zurich *(Mastodon)*
* [basel.social](https://basel.social) - Basel and Basel region *(Mastodon)*
* [mastodon.free-solutions.org](https://mastodon.free-solutions.org) - Switzerland *(Mastodon)*

#### Ukraine
* [soc.ua-fediland.de](https://soc.ua-fediland.de) - Ukraine *(Mastodon)*
* [lviv.social](https://lviv.social) - Lviv *(Mastodon)*

#### UK
* [glasgow.social](https://glasgow.social) - Glasgow *(Mastodon)*
* [bath.social](https://bath.social) - Bath *(Mastodon)*
* [yorkshire.social](https://yorkshire.social) - Yorkshire *(Mastodon)*

#### USA
* [masto.nyc](https://masto.nyc) - New York *(Mastodon)*
* [socialclub.nyc](https://socialclub.nyc) - New York *(Mastodon)*
* [social.tulsa.ok.us](https://social.tulsa.ok.us) - Tulsa, Northeast Oklahoma *(Mastodon)*
* [sfba.social](https://sfba.social) - San Francisco Bay Area *(Mastodon)*
* [mastodon.boston](https://mastodon.boston) - Boston *(Mastodon)*
* [mastodon.miami](https://mastodon.miami) - Miami, Florida *(Mastodon)*
* [https://cityofchicago.live](https://cityofchicago.live) - Chicago, Illinois *(Mastodon)*

### 🐧 [For techies](#servers-for-techies)
* [techlover.eu](https://techlover.eu) - talk about new technologies like development, digital art or science *(Mastodon)*
* [layer8.space](https://layer8.space) - talk about Linux, anime, music, software and more *(Mastodon)*
* [hachyderm.io](https://hachyderm.io) - for respectful professionals in the tech industry *(Mastodon)*
* [devschile.social](https://devschile.social) - for Spanish-language techies of Chile *(Mastodon)*
* [techhub.social](https://techhub.social) - for passionate technologists and everyone in Canada *(Mastodon)*
* [k8s.social](https://k8s.social) - for kubernetes, container and cloud native enthusiasts *(Mastodon)*
* [techspace.social](https://techspace.social) - for techies and tech-curious people *(Mastodon)*
* [gnulinux.social](https://gnulinux.social) -  community dedicated to Free Software enthusiasts and supporters

### 💻 [Programming](#instances-for-programmers)
* [pythondevs.social](https://pythondevs.social) - for Python developers of all experience levels *(Pleroma)*
* [pythonist.as](https://pythonist.as) - for Pythonistas *(Mastodon)*
* [dotnet.social](https://dotnet.social) - .NET *(Mastodon)*

### 🎬 [Book / Game / Show theme](#entertainment)
* [osrs.club](https://osrs.club) - themed around Old School Runescape *(Pleroma)*

### 🔴 [Political and social views](#political-and-social-views)
* [campaign.openworlds.info](https://campaign.openworlds.info) - for campaigners and NGOs *(Mastodon)*

### 🐰 [Environmentalism](#environmentalism)
* [toot.cat](https://toot.cat) - instance for cats, the people who love them, and kindness in general *(Mastodon)*
* [floe.earth](https://mastodon.floe.earth) - talk about climate, society, but also pleasant things in life *(Mastodon)*
* [sauropods.win](https://sauropods.win) - for sauropod appreciators *(Mastodon)*
* [ecocentraal.social](https://ecocentraal.social) - about eco-central world *(Mastodon)*
* [ecoevo.social](https://ecoevo.social) - for the biological ecology and evolution community *(Mastodon)*
* [bhre.social](https://bhre.social) - talk about business, human rights and the environment (BHRE) *(Mastodon)*
* [vegoon.party](https://vegoon.party) -  focus on veganism *(Mastodon)*
* [veganism.social](https://veganism.social) - for vegans and animal rights activists *(Mastodon)*

### ⚕ [Healthcare](#health)
* [fedisabled.social](https://fedisabled.social) - for all disabled people *(Mastodon)*
* [diabetes.masto.host](https://diabetes.masto.host) - safe harbor for people affected by diabetes *(Mastodon)*
* [veterinary.education](https://veterinary.education) - veterinary medicine *(Mastodon)*
* [med-mastodon.com](https://med-mastodon.com) - for medical professionals and curious onlookers *(Mastodon)*

### 👽 [Fandoms](#fandoms)
* [bungle.online](https://bungle.online) - fandom instance, post media, fan art and fanfiction *(Misskey)*
* [mastodol.jp](https://mastodol.jp) - talk about idols in Japanese *(Mastodon)*
* [flower.afn.social](https://flower.afn.social) - talk about flower night girl (フラワーナイトガール) in Japanese *(Mastodon)*

### 🐾 [Subcultures](#subcultures)
* [this.mouse.rocks](https://this.mouse.rocks) - furry instance for all the mouse adjacent folks who rock *(Mastodon)*
* [birds.garden](https://birds.garden) - a quiet, peaceful place where any bird can find refuge *(Pleroma)*
* [stop.voring.me](https://stop.voring.me) - fun hangout for furries and normies alike *(Calckey)*
* [mstdn.beer](https://mstdn.beer) - conscientious adult social gathering place in English/Japanese *(Mastodon)*

### 🌏 [Notable generalistic](#notable-generalistic)
// *small-to-medium sized instances that will be happy to have new users*
* [appalachian.town](https://appalachian.town)
* [venera.social](https://venera.social) - friendly humans are welcome *(Friendica)*
* [ieji.de](https://ieji.de) - has Tor .onion address *(Mastodon)*
* [misskey.de](https://misskey.de) - hosted in Helsinki *(Misskey)*
* [wienermobile.rentals](https://wienermobile.rentals) - for all reasonable people *(Pleroma)*
* [howlr.me](https://howlr.me) - for members of niche and alternative lifestyle communities; originally alterhuman focused *(Akkoma)*
* [social.sp-codes.de](https://social.sp-codes.de) - main language is German *(Mastodon)*
* [toot.io](https://toot.io) - hosted in Germany, mainly in English *(Mastodon)*
* [social.yesterweb.org](https://social.yesterweb.org) - hosted in the USA, mainly in English *(Mastodon)*
* [idlethumbs.social](https://idlethumbs.social) - hosted in California *(Mastodon)*
* [bardown.space](https://bardown.space) - for hockey fans and players *(Hometown)*
* [wetdry.world](https://wetdry.world) - talk about tech, gaming, bad jokes *(Mastodon)*
* [earthlings.socia](https://earthlings.social) - for all precious earthlings *(Mastodon)*
* [troet.crynet.one](https://troet.crynet.one) - English and German, all friendly creatures may join *(Mastodon)*
* [europe.social](https://europe.social) - for users in Europe *(Mastodon)*
* [bytesize.social](https://bytesize.social) - comfortable place to discuss technology, social, political, and beyond *(Mastodon)*
* [masto.bike](https://masto.bike) - riding a bike is a plus, French speaking *(Mastodon)*
* [astronomique.de](https://astronomique.de) - main language is German *(Mastodon)*
* [blueplanet.social](https://blueplanet.social) - for people interested in making this planet a better place, primarily German language
* [convo.casa](https://convo.casa) - general for everyone *(Mastodon)*
* [jistflow.com](https://jistflow.com) - general purpose federated social-verse *(Calckey)*
* [mstodon.eu](https://mstodon.eu) - EU based English language server *(Mastodon)*

### 🐚 [Run by tech-savvy organizations](#run-by-tech-savvy-organizations)
*Instances which declare to be affiliated with an incorporate non-profit (usually an [association](https://en.wikipedia.org/wiki/Voluntary_association) or [cooperative](https://www.ica.coop/en/cooperatives/what-is-a-cooperative)) and to follow its policies*
* [en.osm.town](https://en.osm.town) - for the OpenStreetMap Community *(Mastodon)*
* [pouet.chapril.org](https://pouet.chapril.org) - Chapril/April, free software association, France *(Mastodon)*
* [swiss-chaos.social](https://swiss-chaos.social) - Chaos Computer Club Switzerland *(Mastodon)*

### 🎉 [Notable mention](#notable-mention)
// *are open to particular audience, i.e., to students of university*
* [mastodon.mit.edu](https://mastodon.mit.edu) - for the MIT community *(Mastodon)*
* [metu.life](https://metu.life) - for METU university of Turkey *(Mastodon)*
* [hci.social](https://hci.social) - for human-computer interaction researchers and practitioners, hosted by Princeton University *(Mastodon)*
* [mastodon.librelabucm.org](https://mastodon.librelabucm.org) - for Universidad Complutense de Madrid (UCM) people and free/libre software talk in Spanish *(Mastodon)*
* [mastodon.acc.sunet.se](https://mastodon.acc.sunet.se) - by Academic Computer Club at Umeå University, Sweden *(Mastodon)*
* [akademienl.social](https://akademienl.social) - for anyone working in, affiliated with, or with a general interest in Dutch academia  *(Mastodon)*

### 🎉 [Various (registration by application)](#registration-by-application)
// *these servers may be removed in future automated updates due to signups by application*
* [augsburg.social](https://augsburg.social) - Augsburg, Germany *(Mastodon)*
* [wue.social](https://wue.social) - Würzburg (and the neighbourhood), Germany *(Mastodon)*
* [bonn.social](https://bonn.social) - Bonn, Germany *(Mastodon)*
* [krefeld.life](https://krefeld.life) - Krefeld, Germany *(Mastodon)*
* [darmstadt.social](https://darmstadt.social) - Darmstadt, Germany *(Mastodon)*
* [norden.social](https://norden.social) - Northern Germany *(Mastodon)*
* [snabelen.no](https://snabelen.no) - Norway *(Mastodon)*
* [mastodon.se](https://mastodon.se) - Sweden *(Mastodon)*
* [tukkers.online](https://tukkers.online) - Twente region, Netherlands *(Mastodon)*
* [mastodon.opencloud.lu](https://mastodon.opencloud.lu) - Luxembourg (private and public organisations) *(Mastodon)*
* [mastodon.uy](https://mastodon.uy) - Uruguay *(Mastodon)*
* [chilemasto.casa](https://chilemasto.casa) - Chile *(Mastodon)*
* [mastodon.ie](https://mastodon.ie) - Ireland *(Mastodon)*
* [mastodon.me.uk](https://mastodon.me.uk) - UK tech community *(Mastodon)*
* [mastodos.com](https://mastodos.com) - Kyoto, Japan *(Mastodon)*
* [masto.pt](https://masto.pt) - Portugal *(Mastodon)*
* [scicomm.xyz](https://scicomm.xyz) - for scientists and science enthusiasts *(Mastodon)*
* [fediscience.org](https://fediscience.org) - for publishing scientists, from natural sciences to the humanities *(Mastodon)*
* [imaginair.es](https://imaginair.es) - for writers, painters, cartoonists and people with imagination *(Mastodon)*
* [mograph.social](https://mograph.social) - for motion design community, VFX / 3D artists, animators, illustrators *(Mastodon)*
* [socel.net](https://socel.net) - for animation professionals and freelancers *(Mastodon)*
* [iztasocial.site](https://iztasocial.site) - discussions about psychology for professors of SUAyED Psicología  university of Mexico *(Mastodon)*
* [openbiblio.social](https://openbiblio.social) - German libraries and information facilities *(Mastodon)*
* [ausglam.space](https://ausglam.space) - Australian galleries, libraries, archives, museums and records people *(Mastodon)*
* [linernotes.club](https://linernotes.club) - for discussing music recordings *(Mastodon)*
* [bostonmusic.online](https://bostonmusic.online) - digital space serving the Boston music scene *(Mastodon)*
* [mastodon.radio](https://mastodon.radio) - for Amateur (Ham) Radio community *(Mastodon)*
* [kith.kitchen](https://kith.kitchen) - food and cooking *(Mastodon)*
* [tooot.im](https://tooot.im) - main instance language is Hebrew *(Mastodon)*
* [toot.si](https://toot.si) - Slovenian (multilingual) instance *(Mastodon)*
* [mastodontti.fi](https://mastodontti.fi) - Finnish, public toots should be in Finnish *(Mastodon)*
* [floss.social](https://floss.social) - for people who support or build Free Libre Open Source Software *(Mastodon)*
* [linuxrocks.online](https://linuxrocks.online) - dedicated to Linux and technologies *(Mastodon)*
* [mathstodon.xyz](https://mathstodon.xyz) - for Maths people *(Mastodon)*
* [digipres.club](https://digipres.club) - conversations about digital preservation *(Mastodon)*
* [phpc.social](https://phpc.social) - PHP community
* [eupublic.social](https://eupublic.social) - for people interested in a united, post-national, truly democratic Europe *(Mastodon)*
* [dads.cool](https://dads.cool) - anyone with a kid can be a dad *(Mastodon)*
* [tech.lgbt](https://tech.lgbt) - for tech workers, academics, students, and others in tech who are LGBTQA+ allies *(Mastodon)*
* [colorid.es](https://colorid.es)- Spanish LGBTQIA+ / queer instance primarily for Portuguese speakers *(Mastodon)*
* [fandom.ink](https://fandom.ink) - for fans and fandoms of all types *(Mastodon)*
* [aspiechattr.me](https://aspiechattr.me) - for aspies and other NDs where you can chat to other peeps with ASD, ADHD, and related conditions *(Mastodon)*
* [meow.social](https://meow.social) - general furry/safe-space instance *(Mastodon)*
* [tooting.ch](https://tooting.ch) - generic instance hosted by the FairSocialNet association *(Mastodon)*
* [weirder.earth](https://weirder.earth) - for thoughtful weirdos *(Hometown)*
* [m.g3l.org](https://m.g3l.org) - by G3L, libre software association *(Mastodon)*
* [digitalcourage.social](https://digitalcourage.social) - by DigitalCourage *(Mastodon)*
* [mastodon.nzoss.nz](https://mastodon.nzoss.nz) - by New Zealand Open Source Society *(Mastodon)*
* [mastodon.roflcopter.fr](https://mastodon.roflcopter.fr) - by Roflcopter, [CHATONS](https://chatons.org) member *(Mastodon)*
* [libretooth.gr](https://libretooth.gr) - by LibreOps, who contribute to (re-)decentralizing the net *(Mastodon)*
* [photog.social](https://photog.social) - place for your photos *(Mastodon)*
* [mastodon.cc](https://mastodon.cc) - for art *(Mastodon)*
* [theres.life](https://theres.life) - for Christians *(Mastodon)*
* [fikaverse.club](https://fikaverse.club) - Swedish and other Scandinavian languages *(Mastodon)*
* [wpbuilds.social](https://wpbuilds.social) - for those who love Wordpress and open source *(Mastodon)*
* [feuerwehr.social](https://feuerwehr.social) - for all firefighters in German-speaking countries *(Mastodon)*
* [dragonscave.space](https://dragonscave.space) - instance with a blind and visually impaired membership, welcome to everyone who is a nice person *(Mastodon)*
* [mastodon.gougere.fr](https://mastodon.gougere.fr) - Auxerre, France *(Mastodon)*
* [stereodon.social](https://stereodon.social) - self-managed social network devoted to underground music (Italian, English languages) *(Mastodon)*
* [solarpunk.moe](https://solarpunk.moe) - for solarpunk nerds *(Mastodon)*
* [leftist.network](https://leftist.network) - for those with Leftist politics *(Mastodon)*
* [PCGamer.social](https://pcgamer.social) - by the PC gamer, for the PC gamer *(Mastodon)*
* [poliverso.org](https://poliverso.org) - dedicated to politics and digital rights (Italian speaking) *(Friendica)*
* [rheinland.social](https://rheinland.social) - Rheinland, Germany *(Mastodon)*
* [social.politicaconciencia.org](https://social.politicaconciencia.org) - community about politics, science and culture for Spanish speakers *(Mastodon)*
* [esperanto.masto.host](https://esperanto.masto.host) - for those interested in Esperanto *(Mastodon)*
* [masto.nobigtech.es](https://masto.nobigtech.es) -  for Spanish speakers *(Mastodon)*
* [triangletoot.party](https://triangletoot.party) - Triangle region of North Carolina, USA *(Mastodon)*
* [ruby.social](https://ruby.social) - for people interested in Ruby, Rails and related topics *(Mastodon)*
* [pcgamer.social](https://pcgamer.social) - by PC gamer, for PC gamers *(Mastodon)*
* [activism.openworlds.info](https://activism.openworlds.info) - for activists *(Mastodon)*
* [pipou.academy](https://pipou.academy) - French community with emphasis on kindness *(Mastodon)*
* [mastodontti.fi](https://mastodontti.fi) - for Finnish speakers *(Mastodon)*
* [snabelen.no](https://snabelen.no) - for Norwegian speakers *(Mastodon)*
* [sociale.network](https://sociale.network) - for Italian speakers *(Mastodon)*
* [mastodon.ml](https://mastodon.ml) - Russia *(Mastodon)*
* [cmdr.social](https://cmdr.social) - dedicated to Elite: Dangerous and space exploration *(Mastodon)*
* [fsmi.social](https://fsmi.social) - free software movement of India *(Mastodon)*
* [indieweb.social](https://indieweb.social) - for participants and supporters of the IndieWeb movement *(Mastodon)*
* [handmade.social](https://handmade.social) - for all handmade artisans and their Etsy shops
* [sciencemastodon.com](https://sciencemastodon.com) -  for science journalists and scientists
* [recht.social](https://recht.social) - focus on legal topics *(Mastodon)*
* [medic.cafe](https://medic.cafe) - for employees in the medical field *(Mastodon)*
* [social.cologne](https://social.cologne) - Cologne, Germany *(Mastodon)*
* [graz.social](https://graz.social) - Graz, Austria *(Mastodon)*
* [kcmo.social](https://kcmo.social) - Kansas City, USA *(Mastodon)*
* [leafposter.club](https://leafposter.club) - Canada *(Pleroma)*
* [aus.social](https://aus.social) - Australia *(Mastodon)*
* [sciences.social](https://sciences.social) - for social scientists *(Mastodon)*
* [indiepocalypse.social](https://indiepocalypse.social) - for independent creators of all sorts *(Mastodon)*
* [brettspiel.space](https://brettspiel.space) - for boardgame players *(Mastodon)*
* [podcasts.social](https://podcasts.social) - for podcasters *(Mastodon)*
* [lgbtqia.space](https://lgbtqia.space) - for all LGBTQIA people who want a caring and safe environment *(Mastodon)*
* [red.niboe.info](https://red.niboe.info) - Latin American and Caribbean academic community *(Mastodon)*
* [beo.social](https://beo.social) - Bernese Oberland, Switzerland
* [greennuclear.online](https://greennuclear.online) - nuclear community *(Mastodon)*
* [indiehackers.social](https://indiehackers.social) - for all indie hackers *(Mastodon)*
* [musicians.today](https://musicians.today) - for musicians of all levels, instruments, regions, languages, and genres *(Mastodon)*
* [kalmar.social](https://kalmar.social) - Kalmar region, Sweden *(Mastodon)*
* [blasmusik.social](https://blasmusik.social) - specially, but not only for brass musicians, Germany *(Mastodon)*
* [poketopia.city](https://poketopia.city) - For Pokémon furries, Pokéfurs. Also transformation and inanimate TF. *(Mastodon)*
* [social.seattle.wa.us](https://social.seattle.wa.us) - Seattle *(Mastodon)*


## Disclaimer
`Following instances skipped`: number of users > 5.000, closed registration *at the moment of checking*, behind Cloudflare, running old code, theme / description not understood due to language barrier or scarce description, long blocklists, experimental servers.

__Note__: new server additions welcome, however, this is a curated list for newcomers - for this reason second-level, human-friendly domain names will more likely be accepted than 3-level, long, unintelligible strings of letters.

__Note__: servers with pre-moderated signup process may be placed in "Notable mention" category, only if they represent well-known organizations or establishments. Otherwise they should be listed in the last category "Various (registration by application)".

Add your own themed instance via [merge request](https://codeberg.org/fediverse/fediparty/src/branch/main/source/en/portal/servers/index.md) or send a suggestion to [@lightone@mastodon.xyz](https://mastodon.xyz/@lightone) via Fediverse. If your server already has many users, it will be nice of you to let other servers grow, for a healthier decentralization.
</ul>

## 🌟 Other research links
- [Pirate Parties in Fediverse](https://codeberg.org/lostinlight/distributopia/src/branch/main/caramba)
- [Mastodon server distribution](https://chaos.social/@leah/99837391793032137) research by @Leah

